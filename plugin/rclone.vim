if exists('g:loaded_rclone_nvim')
    finish
endif

let s:old_cpo = &cpo
set cpo&vim

command! -nargs=+ Rclone lua require('rclone').run(unpack({<f-args>}))

let &cpo = s:old_cpo
unlet s:old_cpo

let g:loaded_rclone_nvim = 1

