rclone.nvim
===

Uses [rclone](https://rclone.org/) under the hood.

`copy` or `sync` folders  
or copy the file in the current buffer  
to a remote in your `rclone.conf`

download folders  
or download the file in the current buffer  
from a remote

or `mount` a remote


## Table of Contents

- [Commands](#commands)
- [Usage](#usage)
- [Install](#install)


## Commands

```
:Rclone [<command>] [<remote>] [options]

Arguments:
command		Command to run: [copy, copyFile, download, downloadFile, sync, mount, unmount]
remote		Name of the remote section defined in rclone.conf
		If only one remote is specified that remote is used by default.
		Otherwise the remote with "vim_rclone_default = true" is used.

Options:
--dry-run	Print command to run and don't execute
```


## Usage

Example `rclone.conf`

```
[Test]
type = sftp
host = example.org
user = admin
pass = pa55w0rd
vim_rclone_local_path = .
vim_rclone_remote_path = /test
vim_rclone_mount_directory = testmount

[Test_another]
type = sftp
host = example.org
user = admin
pass = pa55w0rd
vim_rclone_local_path = ./test_another
vim_rclone_remote_path = /test/another
vim_rclone_default = true
```

### copy

`:Rclone copy` copies the local `test_another` folder to the remote `/test/another` because `vim_rclone_default` is set for the **Test_another** remote.

To upload to the **Test** remote use `:Rclone copy Test`.

The resulting command for `:Rclone copy` from the example above would be
```sh
rclone copy $PATH_TO_PROJECT/test_another Test_another:/test/another --config=./rclone.conf
--exclude=.git/ --exclude=.gitignore --exclude=.rcloneignore
--exclude-from=$PATH_TO_PROJECT/test_another/.gitignore --exclude-from=$PATH_TO_PROJECT/test_another/.rcloneignore
--log-level=INFO --log-file=${vim.fn.stdpath('data')}/rclone_nvim/rclone.log
```

#### exclude files

You can extend or overwrite the global `g:vim_rclone_configurable_default_exclude_patterns` variable.  
The default patterns are
`{'.git/', '.gitignore', '.rcloneignore'}`

`rclone.nvim` uses the patterns defined in a `.gitignore` and `.rcloneignore`.

If you want to upload folders that are in the `.gitignore` like a `/build/` folder, then add `!/build/` to your `.rcloneignore`.

**Caution:**

[rclone exclude patterns](https://rclone.org/filtering/#pattern-syntax) for folders **need** a trailing slash.  
If you have ignored folders in your `.gitignore` then you have to add a trailing slash to them, or define them separately in the `.rcloneignore`.

### download

Downloading with `:Rclone download` works the same, the local and remote paths are just swapped like this
```sh
rclone copy Test_another:/test/another $PATH_TO_PROJECT/test_another [...]
```

### mount

Suppose following `rclone.conf`
```
[Test]
type = sftp
host = example.org
user = admin
pass = pa55w0rd
vim_rclone_local_path = ./test_local
vim_rclone_remote_path = /test_remote
vim_rclone_mount_directory = testmount
```

The resulting command for `:Rclone mount` would be
```sh
rclone mount Test:/test_remote $PATH_TO_PROJECT/test_local/testmount --config=./rclone.conf
--log-format=pid --log-file=${vim.fn.stdpath('data')}/rclone_nvim/rclone-mount.log
--read-only --no-checksum --daemon
```

The default mount directory name is `rclone.mount`.  
You can adjust that either by setting the `directory` parameter when running the command with `:Rclone mount directory=othertestmount`,  
within your `rclone.conf`,  
or by setting `g:vim_rclone_mount_directory`.

#### unmount

When running `:Rclone unmount` the pid for the spawned process is identified by
looking for the complete command used when mounting and is just `kill`ed.
So you need to specify the directory parameter as well if it was used when mounting.


## Install

Like any other nvim plugin.

For example with [vim-plug](https://github.com/junegunn/vim-plug)

```vim
Plug 'https://gitlab.com/dweipert.de/rclone.nvim'
```

