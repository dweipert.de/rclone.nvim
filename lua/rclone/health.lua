local health = vim.health or require('health')

local function check()
  if pcall(require, 'plenary') then
    health.report_ok('plenary installed')
  else
    health.report_error('nvim-lua/plenary.nvim missing')
  end

  if vim.fn.executable('rclone') == 1 then
    health.report_ok('rclone installed')
  else
    health.report_error('rclone (rclone.org) not installed')
  end
end

return {
  check = check
}
